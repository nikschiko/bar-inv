@extends("layouts.app")

@section("content")

<!-- From https://bootsnipp.com/snippets/K0ZmK  -->

<div class="container emp-profile">
    <form method="post">
        <div class="row">
            <div class="col-md-4">
                <div class="profile-img">
                    <img src="/img/profile.png" alt=""/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="profile-head">
                            <h5>
                                {{ $user->name }}
                            </h5>
                            <h6>
                                {{ $user->bar }}
                            </h6>
                            <br>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Edit Profile"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-8">
                <div class="tab-content profile-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Benutzername</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{ $user->username }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Bar</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{ $user->bar }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Email</label>
                                    </div>
                                    <div class="col-md-6">
                                        <p>{{ $user->email }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Inventar</label>
                                    </div>
                                    <div class="col-md-6">
                                            <p>
                                                
                                                @if($user->inventory != null)
                                                    <a href="{{ url('/inventory/'. $user->inventory->id) }}">
                                                    {{ $user->inventory->name }}</a>
                                                @else
                                                    <a href="{{ url('/inventory/create') }}">Inventar erstellen</a>
                                                @endif

                                             </p>
                                    </div>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection