@extends('layouts.app')

@section('content')

<div class="container mt-5">
    <form action="/inventory" enctype="multipart/form-data" method="post">
        @csrf
        <div class="row">
            <div class="col-8 offset-2">
            
                <div class="row">
                    <h1>Inventar erstellen</h1>
                </div>

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label">Inventar-Name </label>

                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="description" class="col-md-4 col-form-label">Beschreibung</label>

                        <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required >
                        </textarea>

                        @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="row">
                    <label for="thumbnail" class="col-md-4 col-form-label">Thumbnail</label>
                    <input type="file" class="form-control-file" id="thumbnail" name="thumbnail">
                    @error('thumbnail')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="row mt-3">
                    <button class="btn btn-primary">Erstellen</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection