@extends("layouts.app")

@section("content")

<div class="container mt-5">
    <div class="row">
        <div class="col-md-4">
            <img src="/storage/{{ $inventory->thumbnail }}" style="width: 250px" />
        </div>
        <div class="col-md-8 pt-5">
            <h2 class="mt-3">{{ $inventory->name }}</h2>
            <p class="lead">{{ $inventory->description }}</p>
        </div>
    </div>
    <div class="row mt-3">
        <div class="alert alert-secondary w-100">
            <div class="d-flex justify-content-between">
                <a class="btn btn-success" href="{{ url('/resource/create') }}">Add Item</a>
                <button class="btn btn-success">Filter</button>
            </div>
        </div>
    </div>
    <div class="row">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                <th scope="col">#</th>
                <th scope="col">Produkt</th>
                <th scope="col">Marke</th>
                <th scope="col">Info</th>
                <th scope="col">Preis</th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($inventory->resources as $item)
                <tr>
                    <th scope="row">{{ $loop->index + 1 }}</th>
                    <td>{{ $item->product}}</td>
                    <td>{{$item->manufacturer}}</td>
                    <td>{{$item->info}}</td>
                    <td>{{$item->price.' €'}}</td>
                    <td>
                        <form action="{{ url('/resource', ['id' => $item->id]) }}" method="post">
                            <a href="{{ route('resource.edit', $item->id) }}" class="btn btn-primary">Edit</a>
                            <input class="btn btn-danger" type="submit" value="X" />
                            @method('delete')
                            @csrf
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection