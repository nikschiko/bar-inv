@extends("layouts.app")

@section("content")

<div class="container mt-5">
    <form action="/resource" enctype="multipart/form-data" method="post">
        @csrf
        <div class="row">
            <div class="col-8 offset-2">
            
                <div class="row">
                    <h1>Item hinzufügen</h1>
                </div>

                <div class="form-group row">
                    <label for="product" class="col-md-4 col-form-label">Item Name</label>

                        <input id="product" type="text" class="form-control @error('product') is-invalid @enderror" name="product" value="{{ old('product') }}" required>

                        @error('product')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="manufacturer" class="col-md-4 col-form-label">Hersteller</label>

                        <input id="manufacturer" type="text" class="form-control @error('manufacturer') is-invalid @enderror" name="manufacturer" value="{{ old('manufacturer') }}" required>

                        @error('manufacturer')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="price" class="col-md-4 col-form-label">Preis</label>

                        <input id="price" type="text" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}" required>

                        @error('price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="info" class="col-md-4 col-form-label">Zusätzliche Infos</label>

                        <textarea id="info" type="text" class="form-control @error('info') is-invalid @enderror" name="info" value="{{ old('info') }}" required >
                        </textarea>

                        @error('info')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="row mt-3">
                    <button class="btn btn-primary">Erstellen</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection