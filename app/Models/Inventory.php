<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'description', 'thumbnail'
    ];


    public function inventory() {
        return $this->belongsTo("\App\Models\User");
    }

    public function resources() {
        return $this->hasMany("\App\Models\Resource");
    }

}
