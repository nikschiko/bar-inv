<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    use HasFactory;

    protected $fillable = [
        'product', 'manufacturer', 'info', 'price'
    ];

    public function inventory() {
        return $this->belongsTo("\App\Models\Inventory");
    }

}
