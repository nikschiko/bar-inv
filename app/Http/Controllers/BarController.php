<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Inventory;

class BarController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($inventory)
    {

        $inventory = Inventory::findOrFail($inventory);

        return view('bar.bar', ["inventory" => $inventory]);
    }
}
