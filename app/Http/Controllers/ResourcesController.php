<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Resource;

class ResourcesController extends Controller
{
    
    public function __construct() {
        $this->middleware('auth');
    }

    public function create() {
        return view('bar.resources.add');
    }

    public function store() {

        $input = request()->validate([
            'product'=>'required',
            'manufacturer'=>'required',
            'info'=>'',
            'price'=>'numeric'
        ]);

        auth()->user()->inventory->resources()->create($input);

        return redirect("inventory/".auth()->user()->inventory->id);
    }

    public function edit(Resource $resource)
    {
        return view('bar.resources.edit', compact('resource'));
    }

    public function update(Request $request, Resource $resource) {


        $input = $request->validate([
            'product'=>'required',
            'manufacturer'=>'required',
            'info'=>'',
            'price'=>'numeric'
        ]);

        $resource->update($input);
        return redirect("inventory/".auth()->user()->inventory->id);
    }

    public function destroy($id) {
        \App\Models\Resource::find($id)->delete();
        return \App::make('redirect')->back()->with('flash_success');
    }

}
