<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Inventory;

class InventoriesController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function create() {
        return view('bar.create');
    }

    public function store() {

        $input = request()->validate([
            'name'=>'required',
            'description'=>'required',
            'thumbnail'=>'image'
        ]);

        $imageUrl = "";

        if(isset($input->thumbnail)) {
            $imageUrl = request()->thumbnail->store('uploads', 'public');
        }


        auth()->user()->inventory()->create([
            'name' => $input['name'],
            'description' => $input['description'],
            'thumbnail' => $imageUrl,
        ]);
        
        return redirect('/inventory/' . auth()->user()->inventory->id);
    }

    public function show($id) {
        $inventory = Inventory::findOrFail($id);

        return view('bar.bar', ["inventory" => $inventory]);
    }
}
