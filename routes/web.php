<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", function() {
    return view("landingpage");
});


Auth::routes();

// Inventory Routes

Route::get('/inventory/create', [App\Http\Controllers\InventoriesController::class, 'create']);
Route::post('/inventory', [App\Http\Controllers\InventoriesController::class, 'store']);
Route::get('/inventory/{id}', [App\Http\Controllers\InventoriesController::class, 'show']);

// Resource Routes

Route::resource('resource', \App\Http\Controllers\ResourcesController::class);

Route::get('/profile/{user}', [App\Http\Controllers\ProfilController::class, 'index'])->name('profile.show');
